# uvray

Deployment server for [Ultraviolet](https://gitlab.wikimedia.org/repos/10nm/ultraviolet).
Runs at [ultraviolet.toolforge.org](https://ultraviolet.toolforge.org/) on [Toolforge](https://wikitech.wikimedia.org/wiki/Portal:Toolforge)
