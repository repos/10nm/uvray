import { createServerAdapter } from "@whatwg-node/server";
import { AutoRouter, json } from "itty-router";

import { createServer } from "node:http";
import {
    writeFile,
    mkdtemp,
    mkdir,
    stat,
    rm,
    readdir,
    copyFile,
} from "node:fs/promises";
import { exec as _exec } from "node:child_process";
import { join } from "node:path";
import { promisify } from "node:util";

const exec = promisify(_exec);
const router = AutoRouter();

const BUILDS_DIR = join(process.env.TOOL_DATA_DIR, "www/static/builds");
console.log("using static dir", BUILDS_DIR);

const VALID_ID = /^[a-z0-9-]{1,63}$/;

for (const env of ["TOOL_DATA_DIR", "UV_DEPLOY_TOKEN"]) {
    if (!process.env[env]) {
        console.error("missing env", env);
        process.exit(1);
    }
}

router
    .get("/", () =>
        Response.redirect("https://tools-static.wmflabs.org/ultraviolet/", 302),
    )
    .all("/deployment/:id", (req) => {
        if (req.headers.get("Authorization") !== process.env.UV_DEPLOY_TOKEN) {
            console.error(
                "[!] invalid token:",
                req.headers.get("Authorization"),
                "deployment:",
                req.params.id,
            );
            return json({ error: "unauthorized" }, { status: 401 });
        }

        if (!VALID_ID.test(req.params.id)) {
            console.error("[!] invalid id", req.params.id, req.headers);
            return json({ error: "sussy id" }, { status: 400 });
        }
    })
    .put("/deployment/:id", async (req) => {
        console.log("processing deployment for", req.params.id);

        const buildDir = join(BUILDS_DIR, req.params.id);
        if (await stat(buildDir).catch(() => false)) {
            console.warn("[!] deployment already exists for", req.params.id);
            if (req.params.id.match(/^v\d+-\d+-\d+/)) {
                return json({ error: "already exists" }, { status: 409 });
            }

            await rm(buildDir, { recursive: true });
            console.log("deleted existing build", buildDir);
        }

        const tempDir = await mkdtemp("/tmp/ultraviolet-");
        const archive = join(tempDir, "upload.tar.gz");
        await writeFile(archive, await req.arrayBuffer());

        console.log("wrote uploaded file to", archive);

        await exec(`tar -xzf "${archive}" -C "${tempDir}"`);

        await mkdir(buildDir);
        const files = await readdir(join(tempDir, "dist"));
        for (const file of files) {
            await copyFile(join(tempDir, "dist", file), join(buildDir, file));
        }

        console.log("copied files to", buildDir);

        await rm(tempDir, { recursive: true });

        return json({ ok: true });
    })
    .delete("/deployment/:id", async (req) => {
        console.log("deleting deployment for", req.params.id);

        const buildDir = join(BUILDS_DIR, req.params.id);
        if (!(await stat(buildDir).catch(() => false))) {
            console.error("deployment does not exist for", req.params.id);
            return json({ error: "does not exist" }, { status: 404 });
        }

        await rm(buildDir, { recursive: true });
        console.log("deleted", buildDir);

        return json({ ok: true });
    });

const ittyServer = createServerAdapter(router.fetch);

const httpServer = createServer(ittyServer);
httpServer.listen(process.env.PORT || 8000);
